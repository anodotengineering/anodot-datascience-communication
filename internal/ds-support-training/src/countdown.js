// Import React
import React from 'react';

import CountDown from 'react-native-countdown-component';
import 'moment-timezone';
import Moment from 'react-moment';


const dateToFormat = '1976-04-19T12:59-0500';


export default class Countdown extends React.Component {

    render() {
        return (
                <CountDown until={10} size={20} />
            
        );
    }

}