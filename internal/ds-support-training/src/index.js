import React from 'react';
import ReactDOM from 'react-dom';
import Presentation from './presentation';
//import CountDown from 'react-native-countdown-component';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Presentation />, document.getElementById('root'));
//ReactDOM.render(<CountDown until={10} size={20} />, document.getElementById('root'));
registerServiceWorker();
