// Import React
import React from 'react';

// Import Spectacle Core tags
import {
  Appear,
  BlockQuote,
  Cite,
  Deck,
  Heading,
  ListItem,
  List,
  Quote,
  Slide,
  Text,
  Image,
  Link,

} from 'spectacle';

import Mailto from 'react-protected-mailto'

import Terminal from "spectacle-terminal";
import Typist from "react-typist";
import ReactJson from 'react-json-view'
import Particles from 'react-particles-js';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';

// Import theme
import './presentation.css';
import createTheme from 'spectacle/lib/themes/default';
import particlesTheme from './assets/particlesjs-config.json';

import MetricInvestigationConfig from './assets/MetricInvestigation_1535011411049/MetricInvestigationConfig.json';
import MetricContainer from './assets/MetricInvestigation_1535011411049/MetricContainer_1535011328.json';
import Anomaly from './assets/MetricInvestigation_1535011411049/1m/prod-rcom-server-33c20b8.use.dynamicyield.com.prod-rcom-server-33c20b8-use-dynamicyield-com.rcomserver.postrequesthandler.recommend.mean_90_anomaly.json';
import OfflineMetricModel from './assets/MetricInvestigation_1535011411049/1m/prod-rcom-server-33c20b8.use.dynamicyield.com.prod-rcom-server-33c20b8-use-dynamicyield-com.rcomserver.postrequesthandler.recommend.mean_90_OfflineMetricModel.json';

import anodotLogo from './assets/imgs/anodot_logo.png';
import backgroung_white from './assets/imgs/background_white.png';
import balloons from './assets/imgs/sven-scheuermeier-178631-unsplash.jpg';
import stay_on_path from './assets/imgs/mark-duffel-422279-unsplash.jpg';
import chineese from './assets/imgs/geoff-greenwood-483958-unsplash.jpg';
import path from './assets/imgs/tanner-boriack-538052-unsplash.jpg';
import thats_all_folks from './assets/imgs/thats_all_folks.gif';

// Require CSS
require('normalize.css');


const theme = createTheme(
  {
    primary: 'white',
    secondary: '#1F2022', //black
    tertiary: '#51addc', //'#03A9FC', //blue
    quartenary: '#CECECE', // grey
  },
  {
    primary: 'Montserrat',
    secondary: 'Helvetica',
  }
);

const cursor = { show: false, blink: true, element: "|", hideWhenDone: false, hideWhenDoneDelay: 1000 };

/*<Slide transition={['zoom']} bgColor="primary">
          <Heading size={1} fit caps lineHeight={1} textColor="secondary">
            Spectacle Boilerplate
          </Heading>
          <Text margin="10px 0 0" textColor="tertiary" size={1} fit bold>
            open the presentation/index.js file to get started
          </Text>
        </Slide>
        <Slide transition={['fade']} bgColor="tertiary">
          <Heading size={6} textColor="primary" caps>
            Typography
          </Heading>
          <Heading size={1} textColor="secondary">
            Heading 1
          </Heading>
          <Heading size={2} textColor="secondary">
            Heading 2
          </Heading>
          <Heading size={3} textColor="secondary">
            Heading 3
          </Heading>
          <Heading size={4} textColor="secondary">
            Heading 4
          </Heading>
          <Heading size={5} textColor="secondary">
            Heading 5
          </Heading>
          <Text size={6} textColor="secondary">
            Standard text
          </Text>
        </Slide>
        <Slide transition={['fade']} bgColor="primary" textColor="tertiary">
          <Heading size={6} textColor="secondary" caps>
            Standard List
          </Heading>
          <List>
            <ListItem>Item 1</ListItem>
            <ListItem>Item 2</ListItem>
            <ListItem>Item 3</ListItem>
            <ListItem>Item 4</ListItem>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor="secondary" textColor="primary">
          <BlockQuote>
            <Quote>Example Quote</Quote>
            <Cite>Author</Cite>
          </BlockQuote>
        </Slide>
        
         <Particles  params={particlesTheme} /> 

         <Table>
          <TableHeader>
            <TableRow>
            <TableHeaderItem>Rollup</TableHeaderItem>
              <TableHeaderItem>TimeScale</TableHeaderItem>
            </TableRow>
          </TableHeader>
            <TableBody>
              <TableRow>
                <TableItem>SHORTROLLUP</TableItem>
                <TableItem>1m</TableItem>
              </TableRow>
              <TableRow>
                <TableItem>MEDIUMROLLUP</TableItem>
                <TableItem>5m</TableItem>
              </TableRow>
              <TableRow>
                <TableItem>LONGROLLUP</TableItem>
                <TableItem>1h</TableItem>
              </TableRow>
              <TableRow>
                <TableItem>LONGLONGROLLUP</TableItem>
                <TableItem>1d</TableItem>
              </TableRow>
              <TableRow>
                <TableItem>WEEKLY</TableItem>
                <TableItem>1w</TableItem>
              </TableRow>
            </TableBody>
          </Table>
        
        */




       

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck transition={['zoom', 'slide']} transitionDuration={500} theme={theme} progress="bar" contentHeight="1000">
        
        <Slide transition={['zoom']} bgColor="primary" bgImage={backgroung_white}>
            <Image src={anodotLogo}/>
            <Heading size={1} fit caps lineHeight={1} textColor="secondary">
              Data science support training
            </Heading>
            <Text margin="10px 0 0" textColor="tertiary" size={2} fit bold>
              Some guideline to improve the support process
            </Text>


            <Text margin="10px 0 0" textColor="tertiary" size={6}>
              Meir TOLEDANO, Data scientist
            </Text>

            <Text margin="10px 0 0" textColor="tertiary" size={6} >
              <Mailto textColor="tertiary" email="meir@anodot.com" />
            </Text>
        </Slide>

        <Slide transition={['fade']} bgColor="tertiary">
          <Heading size={3} fit caps lineHeight={1} textColor="primary">
            What we want to improve ?
          </Heading>
          <List textColor="secondary">
            <ListItem>Speak the same language</ListItem>
            <ListItem>Faster answer to customer</ListItem>
            <ListItem>Improve scalability</ListItem>
            <ListItem>Readiness for hypergrowth</ListItem>
          </List>
        </Slide>

        <Slide transition={['fade']} bgImage={chineese}>
          <Heading size={1} fit caps lineHeight={1} textColor="quartenary">
            Speak the same language
          </Heading>
        </Slide>

        <Slide transition={['fade']} bgColor="primary" controlColor="tertiary" progressColor="tertiary">
          <Heading size={2} fit caps lineHeight={1} textColor="tertiary">
            Request template
          </Heading>
          <Text textAlign="left" textSize="20px">
            <ReactJson src={MetricInvestigationConfig} theme="rjv-default" />
          </Text>
        </Slide>


         <Slide transition={['fade']} bgColor="primary" controlColor="tertiary" progressColor="tertiary">
          <Heading size={2} fit caps lineHeight={1} textColor="tertiary">
            How to fill the template ?
          </Heading>
          <Heading size={3} fit lineHeight={1} textColor="secondary">
          environment
          </Heading>
          <List>
            <ListItem>PRODUCTION</ListItem>
            <ListItem>POC</ListItem>
            <ListItem>MONITORING</ListItem>
          </List>
        </Slide>


        <Slide transition={['fade']} bgColor="primary" controlColor="tertiary" progressColor="tertiary">
          <Heading size={2} fit caps lineHeight={1} textColor="tertiary">
          How to fill the template ?
          </Heading>
          <Heading size={3} fit lineHeight={1} textColor="secondary">
          token / userId
          </Heading>
          <Text margin="10px 0 0" textColor="secondary" size={6} fit bold>
            Only one of them is neccessary
          </Text>
        </Slide>


        <Slide transition={['fade']} bgColor="primary" controlColor="tertiary" progressColor="tertiary">
          <Heading size={2} fit caps lineHeight={1} textColor="tertiary">
          How to fill the template ?
          </Heading>
          <Heading size={3} fit lineHeight={1} textColor="secondary">
            metricName / metricId
          </Heading>
          <Text margin="10px 0 0" textColor="secondary" size={6} fit bold>
            Only one of them is neccessary
          </Text>
        </Slide>


        <Slide transition={['fade']} bgColor="primary" controlColor="tertiary" progressColor="tertiary">
          <Heading size={2} fit caps lineHeight={1} textColor="tertiary">
            How to fill the template ?
          </Heading>
          <Heading size={3} fit lineHeight={1} textColor="secondary">
            anomalyId
          </Heading>
          <Text margin="10px 0 0" textColor="secondary" size={6} fit bold>
            Describe the anomaly we are talking about. Optional.
          </Text>
          <Image src="https://sites.google.com/a/anodot.com/architecture/_/rsrc/1534172300159/analytics/metric-investistigation-how-to/anomalyId.png"/>
        </Slide>


        <Slide transition={['fade']} bgColor="primary" controlColor="tertiary" progressColor="tertiary">
          <Heading size={2} fit caps lineHeight={1} textColor="tertiary">
            How to fill the template ?
          </Heading>
          <Heading size={3} fit lineHeight={1} textColor="secondary">
            rollups
          </Heading>
          <Text margin="10px 0 0" textColor="secondary" size={6} fit bold>
            A list of rollups from
          </Text>
        
              <Paper>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell padding="checkbox"><Checkbox  /></TableCell>
                      <TableCell>Rollup</TableCell>
                      <TableCell>TimeScale</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                        <TableRow>
                          <TableCell padding="checkbox"><Checkbox  /></TableCell>
                          <TableCell component="th" scope="row">SHORTROLLUP</TableCell>
                          <TableCell>1m</TableCell>
                        </TableRow>

                        <TableRow>
                          <TableCell padding="checkbox"><Checkbox  /></TableCell>
                          <TableCell component="th" scope="row">MEDIUMROLLUP</TableCell>
                          <TableCell>5m</TableCell>
                        </TableRow>

                        <TableRow>
                          <TableCell padding="checkbox"><Checkbox  /></TableCell>
                          <TableCell component="th" scope="row">LONGROLLUP</TableCell>
                          <TableCell>1h</TableCell>
                        </TableRow>

                        <TableRow>
                          <TableCell padding="checkbox"><Checkbox  /></TableCell>
                          <TableCell component="th" scope="row">LONGLONGROLLUP</TableCell>
                          <TableCell>1d</TableCell>
                        </TableRow>

                        <TableRow>
                          <TableCell padding="checkbox"><Checkbox  /></TableCell>
                          <TableCell component="th" scope="row">WEEKLY</TableCell>
                          <TableCell>1w</TableCell>
                        </TableRow>
                  </TableBody>
                </Table>
              </Paper>
        </Slide>

        <Slide transition={['fade']} bgImage={path}>
          <Heading size={1} fit caps lineHeight={1} textColor="quartenary">
            Process
          </Heading>
        </Slide>

        <Slide transition={['fade']} bgColor="primary" controlColor="tertiary" progressColor="tertiary">
          <Heading size={2} fit lineHeight={1} textColor="tertiary">
            How to open a case ?
          </Heading>
          <List>
            <ListItem>This will be the standard language between CS / DS </ListItem>
            <ListItem>Use the <Link href="https://anodot.atlassian.net/secure/RapidBoard.jspa?projectKey=SDSE&rapidView=25">JIRA SDSE board</Link> in priority. (Statistics tracking)</ListItem>
            <ListItem>Please don't us mail or Slack</ListItem>
            <ListItem>We are advising to use the chrome add-on <Link href="https://chrome.google.com/webstore/detail/json-editor/lhkmoheomjbkfloacpgllgjcamhihfaj?hl=en">JSON Editor</Link></ListItem>
            <ListItem>Lost? We have a wiki <Link href="https://sites.google.com/a/anodot.com/architecture/analytics/metric-investistigation-how-to">Anodot Wiki</Link></ListItem>
          </List>
        </Slide>

        <Slide transition={['fade']} bgImage={stay_on_path}> </Slide>

        <Slide transition={['fade']} bgImage={balloons}>
          <Heading size={1} fit caps lineHeight={1} textColor="primary">
            Toward autonomy
          </Heading>
        </Slide>

        <Slide transition={['fade']} bgColor="tertiary" controlColor="primary" progressColor="primary">
          <Heading size={2} fit lineHeight={1} textColor="primary">
            State of the process
          </Heading>
          <List>
            <ListItem>The process is slow</ListItem>
            <ListItem>Lots of question related to the same subject : Score, Season...</ListItem>
            <ListItem>80/20 rule: 80% of problem can be solved by youreself easily</ListItem>
            <ListItem>It is faster for everyone (Customer, CS, DS) if you know how to solve those simple case</ListItem>
          </List>
        </Slide>

        <Slide transition={['fade']} bgColor="primary" controlColor="tertiary" progressColor="tertiary">
          <Heading size={2} fit lineHeight={1} textColor="tertiary">
            Solution
          </Heading>
          <List>
            <ListItem>We are releasing the MetricInvestigation tool. You can query our database for reading what happen to the baseline in the online, offline and anomaly index.</ListItem>
            <ListItem>Not super user friendly</ListItem>
            <ListItem>Work currently only in Israel office</ListItem>
            <ListItem>Let's try if it has value</ListItem>
          </List>
        </Slide>


        <Slide transition={['fade']} bgColor="tertiary" controlColor="primary" progressColor="primary">
          <Heading size={2} fit lineHeight={1} textColor="primary">
            MetricInvestigation setup
          </Heading>
          <List ordered>
            <ListItem>Download the jar from <Link href="https://drive.google.com/file/d/1T9poP_S_SGSfo6VZomABkwHn-vIVTBGx/view">here</Link> and put it in {'{jar-directory}'} (of your choice)</ListItem>
            <ListItem>Create an output directory {'{output-directory}'} for the results of the program</ListItem>
            <ListItem>Run the tool from console</ListItem>
          </List>
        </Slide>


         <Slide transition={['fade']} bgColor="primary" controlColor="tertiary" progressColor="tertiary">
          <Heading size={2} fit lineHeight={1} textColor="tertiary">
            MetricInvestigation how-to
          </Heading>
          <Terminal title="my console" fontSize="0.75vw" output={[
                <Typist cursor={ cursor }>java -cp {'{jar-directory}'}/anodot_research.jar io.anodot.ml.research.connected.real.MetricInvestigation -c {'{config-directory}'}/MetricInvestigationConfig.json {'{output-directory}'}</Typist>,
                <div>Run MetricInvestigation_1535289794382</div>,
                <div>
                  <div style={{ color: "#E74C3C"}}>... Lots of errors ...</div>
                  <div style={{ color: "#2ECC71"}}>Cassandra prepareStmt completed !</div>
                </div>,
                <div>
                  <div>### Configuration</div>
                  <div>### MetricKey</div>
                  <div>### MetricContainer</div>
                  <div>### OfflineMetricModel 1m</div>
                  <div>### Baseline 1m</div>
                  <div>### Season 1m</div>
                  <div>### Anomaly </div>
                </div>,
                <div style={{ color: "#2ECC71"}}>Done !</div>
              ]}
            />
        </Slide>

        <Slide transition={['fade']} bgColor="tertiary" controlColor="primary" progressColor="primary">
          <Heading size={2} fit lineHeight={1} textColor="primary">
            MetricInvestigation output description (partial)
          </Heading>
          <List>
            <Appear><ListItem>Configuration: The configuration for the client</ListItem></Appear>
            <Appear><ListItem>MetricContainer: The snapshot of the memory of the online process</ListItem></Appear>
            <Appear><ListItem>timeScale/baseline.csv: The raw data and the baseline</ListItem></Appear>
            <Appear><ListItem>timeScale/OfflineMetricModel: The last result of the offline model</ListItem></Appear>
            <Appear><ListItem>timeScale/anomaly.json: The details of the anomaly in case of anomalyId is not null</ListItem></Appear>
          </List>
        </Slide>

        <Slide transition={['fade']} bgColor="primary" controlColor="tertiary" progressColor="tertiary">
            <Heading size={2} fit lineHeight={1} textColor="tertiary">
              MetricContainer
            </Heading>
            <Text textAlign="left" textSize="20px">
              <ReactJson src={MetricContainer} theme="rjv-default" />
            </Text>
        </Slide>

        <Slide transition={['fade']} bgColor="primary"controlColor="tertiary" progressColor="tertiary">
            <Heading size={2} fit lineHeight={1} textColor="tertiary">
              OfflineMetricModel
            </Heading>
            <Text textAlign="left" textSize="20px">
              <ReactJson src={OfflineMetricModel} theme="rjv-default" />
            </Text>
        </Slide>

        <Slide transition={['fade']} bgColor="primary"controlColor="tertiary" progressColor="tertiary">
            <Heading size={2} fit lineHeight={1} textColor="tertiary">
              Anomaly
            </Heading>
            <Text textAlign="left" textSize="20px">
              <ReactJson src={Anomaly} theme="rjv-default" />
            </Text>
        </Slide>

       <Slide transition={['fade']} bgImage={thats_all_folks}>
        </Slide>

      </Deck>
    );
  }
}
